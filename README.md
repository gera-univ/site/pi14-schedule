Расписание для 1 и 2 курса ПИ

Используется *bootstrap v4.3.1*

## Адреса

[fpmi.spiralarms.org](https://fpmi.spiralarms.org) - gitlab pages (рекомендуется)

[light.fpmi.spiralarms.org](https://light.fpmi.spiralarms.org/) - зеркало сайта с оптимизированными настройками кеширования

### Cтарые адреса
- [bit.ly/pi14-sc](https://bit.ly/pi14-sc)

- [pivo.spiralarms.org](https://pivo.spiralarms.org)

- [pishki.spiralarms.org](https://pishki.spiralarms.org)

## Цвета

| тип пары | класс bootstrap | код цвета |
| ------ | ------ | ------ |
| лекция | list-group-item-info |`#bee5eb`
| практика | list-group-item-secondary | `#d6d8db`
| физра |  list-group-item-danger | `#f5c6cb`
| форточка |  list-group-item-success | `#c3e6cb`