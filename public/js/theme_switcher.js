import Cookies from './js.cookie.mjs'

const cssDay = "css/common.css";
const cssNight = "css/common_night.css";
var cssPathPrefix = "";

var loc = window.location.pathname;
var dir = loc.substring(0, loc.lastIndexOf('/'));
console.log(dir);
if (dir === "/pages")
    cssPathPrefix = "../";

jQuery(document).ready(function ($) {
    $("#theme-switcher-checkbox").on("change", onSwitchCss)
});

var cssPath = Cookies.get("css_path");
if (!cssPath) {
    cssPath = cssDay;
    Cookies.set("css_path", cssDay);
}

function onSwitchCss() {
    if (cssPath === cssDay)
        cssPath = cssNight;
    else
        cssPath = cssDay;
    Cookies.set("css_path", cssPath);
    changeCss(cssPathPrefix + cssPath);
    setTimeout(function () {
        location.reload();
    }, 500);
}

function changeCss(cssPath) {
    console.log("theme_switcher: loading " + cssPath);

    var head = document.getElementsByTagName('head')[0];
    var link = document.createElement('link');
    link.id = cssPath;
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = cssPath;
    link.media = 'all';
    head.appendChild(link);
}

changeCss(cssPathPrefix + cssPath);
