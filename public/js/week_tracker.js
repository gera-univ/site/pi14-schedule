function getMonday(d) {
    d = new Date(d);
    var day = d.getDay(),
        diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
    return new Date(d.setDate(diff));
}

Date.prototype.addDays = function (days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
};

function dayMonthString(d) {
    function pad(n) {
        return n < 10 ? '0' + n : n
    }

    return pad(d.getUTCDate()) + '.' + pad(d.getUTCMonth() + 1)
}

function daysBetween(first, second) {

    // Copy date parts of the timestamps, discarding the time parts.
    var one = new Date(first.getFullYear(), first.getMonth(), first.getDate());
    var two = new Date(second.getFullYear(), second.getMonth(), second.getDate());

    // Do the math.
    var millisecondsPerDay = 1000 * 60 * 60 * 24;
    var millisBetween = two.getTime() - one.getTime();
    var days = millisBetween / millisecondsPerDay;

    // Round down.
    return Math.floor(days);
}

function checkWeek(json) {
    date = new Date();
    dateMonday = getMonday(date);

    dateWeekStart = new Date(json[$('body').attr('id')][0]);

    if (daysBetween(dateMonday, dateWeekStart) < 0) {
        console.log("Расписание отстаёт!");
        $("#text-schedule-out-of-date").text("Расписание отстаёт!");
        $("#alert-schedule-out-of-date").addClass("alert-danger");
    } else if (daysBetween(dateMonday, dateWeekStart) > 0) {
        console.log("Расписание спешит!");
        $("#text-schedule-out-of-date").text("Расписание спешит!");
        $("#alert-schedule-out-of-date").addClass("alert-info");
    } else {
        $("#text-schedule-out-of-date").remove();
        $("#alert-schedule-out-of-date").addClass("alert-success");
    }

    $("#week-schedule-out-of-date").text(dayMonthString(dateWeekStart) + "-" + dayMonthString(dateWeekStart.addDays(6)));

    function changeDateLabel(dateLabelID, dateWeekday) {
        $(dateLabelID).text(dayMonthString(dateWeekday));
        $(dateLabelID).removeClass("badge-light");
        if (daysBetween(dateWeekday, date) < 0)
            $(dateLabelID).addClass("badge-info");
        else if (daysBetween(dateWeekday, date) > 0)
            $(dateLabelID).addClass("badge-secondary");
        else
            $(dateLabelID).addClass("badge-warning");
    }

    changeDateLabel("#date-monday", dateWeekStart);
    changeDateLabel("#date-tuesday", dateWeekStart.addDays(1));
    changeDateLabel("#date-wednesday", dateWeekStart.addDays(2));
    changeDateLabel("#date-thursday", dateWeekStart.addDays(3));
    changeDateLabel("#date-friday", dateWeekStart.addDays(4));
    changeDateLabel("#date-saturday", dateWeekStart.addDays(5));
    changeDateLabel("#date-sunday", dateWeekStart.addDays(6));
}

$.ajaxSetup ({
    // Disable caching of AJAX responses
    cache: false
});

jQuery(document).ready(function ($) {
    $.getJSON("./weeks.json", function (json) {
        checkWeek(json);
    })
});
