const today = new Date();
const birthdaysCount = 3;

function compareThirdColumn(a, b) {
    if (a[2] === b[2]) {
        return 0;
    } else {
        return (a[2] < b[2]) ? -1 : 1;
    }
}

function dayMonthYearString(d) {
    function pad(n) {
        return n < 10 ? '0' + n : n
    }

    return pad(d.getUTCDate() + 1) + '.' + pad(d.getUTCMonth() + 1) + '.' + pad(d.getUTCFullYear())
}

function daysBetween(first, second) {

    // Copy date parts of the timestamps, discarding the time parts.
    const one = new Date(first.getFullYear(), first.getMonth(), first.getDate());
    const two = new Date(second.getFullYear(), second.getMonth(), second.getDate());

    // Do the math.
    const millisecondsPerDay = 1000 * 60 * 60 * 24;
    const millisBetween = two.getTime() - one.getTime();
    const days = millisBetween / millisecondsPerDay;

    // Round down.
    return Math.floor(days);
}

function getBirthday(birthDate) {
    birthDate.setFullYear(today.getFullYear());
    if (daysBetween(today, birthDate) < 0)
        birthDate.setFullYear(today.getFullYear() + 1);
    return birthDate
}

jQuery(document).ready(function ($) {
    const birthdaysFilename = "./birthdays/" + document.body.id + ".csv";
    $.ajax({
        type: "GET",
        url: birthdaysFilename,
        dataType: "text",
        success: function (response) {
            let entry;
            let data = $.csv.toArrays(response);

            for (let i = 0; i < data.length; ++i) {
                const birthDateLocal = new Date(data[i][1].split("-").reverse().join("-"));
                const userOffset = birthDateLocal.getTimezoneOffset()*60000;
                const birthDate = new Date(birthDateLocal.getTime() + userOffset);
                const birthday = getBirthday(birthDate);

                data[i][1] = birthday;

                data[i].push(daysBetween(today, birthday));
            }

            data.sort(compareThirdColumn);

            let birthdayHappened = false;
            for (let i = 0; i < birthdaysCount; ++i) {
                if (data[i][2] === 0) {
                    entry = $("<li>", {"class": "list-group-item list-group-item-success"});
                    entry.text(data[i][0] + " сегодня!");
                    birthdayHappened = true;
                } else {
                    entry = $("<li>", {"class": "list-group-item"});

                    let daysWord = "дней";
                    if (!(data[i][2] > 10 && data[i][2] < 15)) {
                        const rem = data[i][2] % 10;
                        if (rem === 1)
                            daysWord = "день";
                        else if (rem === 2 || rem === 3 || rem === 4)
                            daysWord = "дня";
                    }

                    entry.text(data[i][0] + " " + dayMonthYearString(data[i][1]) + " через " + data[i][2] + " " + daysWord + "!");
                }

                $("#birthdays-list").append(entry);
                if (birthdayHappened) {
                    $("#notification-alarm").attr("src", "img/notification-on.svg");

                    var confettiSettings = { target: 'confetti' };
                    var confetti = new ConfettiGenerator(confettiSettings);
                    confetti.render();
                }
            }
        }
    });
});
